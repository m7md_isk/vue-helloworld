import Vuex from "vuex";

const store = new Vuex.Store({
  state: {
    todos: {},
  },
  mutations: {
    addTodo(state, { name, done }) {
      state.todos.push({id:state.todos.length,name:name,done:done})
    },
    toggleTodo(state, id) {
      let idx = state.todos.findIndex(el => el.id = id )
      state.todos[idx].done = !state.todos[idx].done

    },
    setTodos(state, td) {
      state.todos = td;
    },
  },
  actions: {
    fetchTodos(store) {
      return fetch("http://localhost:3000/todos")
        .then((res) => res.json())
        .then((res) => {
          store.commit("setTodos", res);
        });
    },
  },
});
export default store;
